import CamelCase
import unittest

class Test_Camelcase(unittest.TestCase):
    def test_type_saveChangesInTheEditor_shoud_be_5(self):
        self.assertEqual(CamelCase.camelcase('saveChangesInTheEditor'), 5, 'Should be 5')

    def test_type_hiIAmStundentOfPsu_shoud_be_5(self):
        self.assertEqual(CamelCase.camelcase('hiIAmStundentOfPsu'), 6, 'Should be 6')
    
    def test_read_input01_shoud_be_1(self):
        simpleFile = open("Testcase\input01.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text), 1, 'Should be 1')
        simpleFile.close()
    
    
    def test_read_input04_shoud_be_2(self):
        simpleFile = open("Testcase\input04.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text), 2, 'Should be 2')
        simpleFile.close()
    
    def test_read_input05_shoud_be_1(self):
        simpleFile = open("Testcase\input05.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text), 1, 'Should be 1')
        simpleFile.close()
    
    def test_read_input07_shoud_be_4588(self):
        simpleFile = open("Testcase\input07.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text ), 4588, 'Should be 4588')
        simpleFile.close()
 
    def test_read_input08_shoud_be_3199(self):
        simpleFile = open("Testcase\input08.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text ), 3199, 'Should be 3199')
        simpleFile.close()

    def test_read_input09_shoud_be_1864(self):
        simpleFile = open("Testcase\input09.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text ), 1864, 'Should be 1864')
        simpleFile.close()

    def test_read_input10_shoud_be_4073(self):
        simpleFile = open("Testcase\input10.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text), 4073, 'Should be 4073')
        simpleFile.close()
    
    def test_read_input19_shoud_be_4073(self):
        simpleFile = open("Testcase\input19.txt", "r")
        read_text = simpleFile.read()
        self.assertEqual(CamelCase.camelcase(read_text), 4215, 'Should be 4215')
        simpleFile.close()

if __name__ == '__main__':
   unittest.main()